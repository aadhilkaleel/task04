import java.lang.reflect.Field;

public class Reflection05 {
	public static void main(String[] args) throws Exception {
		
	    RSample sample = new RSample();
	    
	    System.out.println("Reflection05");
	    
	    Field[] fields = sample.getClass().getDeclaredFields();
	    
	    System.out.printf("%d fields are running...\n", fields.length);

	    for (Field f : fields) {
	    	
	    	System.out.printf("field name = %s \ttype = %s  \tvalue = %.2f\n", f.getName(), 
	  	          f.getType(), f.getDouble(sample));
	    	
	    }
	  }
}
