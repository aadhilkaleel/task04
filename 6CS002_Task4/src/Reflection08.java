import java.lang.reflect.Field;

public class Reflection08 {
	
	public static void main(String[] args) throws Exception {
		
	    RSample sample = new RSample();
	    System.out.println("Reflection08");
	    
	    Field[] fields = sample.getClass().getDeclaredFields();
	    
	    System.out.printf("%d fields are running...\n", fields.length);
	    
	    for (Field f : fields) {
	    	
	      f.setAccessible(true);
	      Double num = f.getDouble(sample);
	      num++;
	      f.setDouble(sample, num);
	      System.out.printf("field name = %s \ttype = %s  \tvalue = %.2f\n", f.getName(), 
	          f.getType(), f.getDouble(sample));
	    }
	  }

}
